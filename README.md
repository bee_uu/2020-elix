### Saturday, April 4th 2020, 18:00 UTC (19:00 CEST)

# 2020-eLix #2: Two-Stage Duel

### Bracket

[Test Bracket to demonstrate format](https://challonge.com/2020_elix_2)

### Streams

[mxCam](https://twitch.tv/mxcam)
[Draena](https://twitch.tv/sl4cer)
[Dodger](https://twitch.tv/DodgerXon)

### Results

## Game Settings

**Game Mode:** Duel  
**Ruleset:** XPM  
**Timelimit:** 10 minutes  
**Frag Limit:** None  


The changes from standard XPM settings are:

* You only have a 40% chance of spawning at the furthest point from your enemy. This is a try to prevent people forcing the enemy to spawn in a certain spot and insta-killing them.
* There is a minimum respawn delay of 1 second, with a maximum of 4 seconds.
* If there is a tie after 10 minutes there will be an overtime of 2 minutes. A maximum of 5 overtimes can take place if there is still no winner, afterwards there is sudden death.

See also `elix-base.cfg` and `elix-duel.cfg` in this repo.  

### Map Pool

quark, stormkeep, silentsiege, graphite, tuhma, hub, fuse

Each duel cup, one map in the pool will be replaced by a fresh one.

## Format

The format will be a 2-stage tournament with a group stage.

### Groupstage

GSL Style Groups (bo3) of 4 players. 
Basically the groupstage will be a mini-bracket with 4 players each. Top two will advance to playoffs.
See this image for how it works. Or click the bracket link and try to follow that :).

![gsl-style-group-bracket](https://tl.net/staff/Plexa/groupphase2.PNG)

### Playoffs

Single Elimination bo3 with match for 3rd place.  
The finals will be bo5.

### Map Vetoes

##### bo3

*drop* drop *pick* pick *drop* drop, remaining map is decider

##### bo5

*drop* drop *pick* pick *pick* pick, remaining map is decider

*italic* = winner of cointoss  
normal text = loser of cointoss


## Prize Money

1st: 26€  
2nd: 16€  
3rd: 8€  

I'm open to donations for more prize money.

## Rules

I'm mostly trusting that people play fair here and won't publish a 200 page rule book. Nevertheless I want to make a few things clear:

* Sign up in this thread or on [IRC](https://webchat.quakenet.org/?channels=xonotic.cup) with any of the admins, then Check-In before 18:00 UTC. Late check-ins can not be considered.
* Admins have the final say in any controversial decisions

### Disconnects

* If a player disconnects or quits before 2:00 minutes have been played, the map will be restarted. Abuse of this will be punished with disqualification.
* If more than 2:00 minutes have passed, the player that disconnected will lose the map by default.
* If both players agree, the game can be restarted.

### Time/Date

Check-in starts Saturday, April 4th 2020, 17:00 UTC (18:00 CET)

Play starts at 18:00 UTC (19:00 CET)

## Channels

[IRC](https://webchat.quakenet.org/?channels=xonotic.cup)

[Discord](https://discord.gg/CJwA4b2)

## Admins

bw, Mirio, morosophos, packer


### Servers

The servers will be amazing. They will run on a 125hz ticrate (most servers use 30-60) and be hosted on amazon AWS with plenty of power in Frankfurt.
packer has kindly agreed to admin the servers.

| IP  | Name |
|-----|---------------------------------|
| TBA |	[2020-eLix] Duel Cup (A1) (GER) |
| TBA |	[2020-eLix] Duel Cup (A2) (GER) |
| TBA |	[2020-eLix] Duel Cup (B1) (GER) |
| TBA |	[2020-eLix] Duel Cup (B2) (GER) |
| TBA |	[2020-eLix] Duel Cup (C1) (GER) |
| TBA |	[2020-eLix] Duel Cup (C2) (GER) |
| TBA |	[2020-eLix] Duel Cup (D1) (GER) |
| TBA |	[2020-eLix] Duel Cup (D2) (GER) |
| TBA |	[2020-eLix] Duel Cup (E1) (GER) |
| TBA |	[2020-eLix] Duel Cup (E2) (GER) |
| TBA |	[2020-eLix] Duel Cup (F1) (GER) |
| TBA |	[2020-eLix] Duel Cup (F2) (GER) |

## Participants
